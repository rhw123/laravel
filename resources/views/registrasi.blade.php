<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>

<h1>Buat Account Baru!</h1>

<h4>Sign Up Form</h4>

<form action="/welcome" method="POST">
    @csrf
    <label for="">First name</label><br><br>
    <input type="text" name="firstname"><br><br> 

    <label for="">Last name</label><br><br>
    <input type="text" name = "lastname"><br><br>

    <label for=""> Gender : </label><br><br>
    <input type="radio" name="radio" value="male"><label for="" >Male</label><br>
    <input type="radio" name="radio" value="female"><label for="">Female</label><br>
    <input type="radio" name="radio" value="other"><label for="">Other</label><br><br>

    <label for="">Nationality:</label><br><br>
    <select name=" ck ">
        <option value="Indonesia">Indonesia</option>
        <option value="Jepang">Jepang</option>
        <option value="Inggris">Inggris</option>
    </select><br><br>

    <label for="">Language Spoken</label><br><br>
    <input type="checkbox" name="ck1"><label for="">Bahasa Indonesia</label><br>
    <input type="checkbox" name="ck2"><label for="">English</label><br>
    <input type="checkbox" name="ck3"><label for="">Others</label><br><br>

    <label for="">Bio</label><br><br>
    <textarea name="txtarea" id="" cols="30" rows="10"></textarea><br><br>

    <button name="daftar">Daftar</button>
</form>
    
</body>
</html>